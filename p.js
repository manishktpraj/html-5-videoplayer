document.addEventListener("DOMContentLoaded", function() { startplayer(); }, false);
var player;
var playd=0;
function startplayer() 
{
 player = document.getElementById('video_player');
 player.controls = false;
}
function play_vid()
{
	if(playd==0)
	{
 player.play();
 document.getElementById('play_button').src="images/pause-symbol.png";
 playd=1;
 player.addEventListener('timeupdate', updateProgressBar, false);

	}else{
		player.pause();
		 document.getElementById('play_button').src="images/play-button.png";
		 playd=0;
	}
	console.log(player.currentTime);
}
 
function stop_vid() 
{
 player.pause();
 player.currentTime = 0;
}
function change_vol()
{
 player.volume=document.getElementById("change_vol").value;
}
function setqueality()
{
	player.setAttribute('src','https://player.vimeo.com/external/356694483.sd.mp4?s=039fcbe7537248f1fae751c5935c69dc87c38656&profile_id=164');
	
}

function raisespeed()
{
	player.playbackRate =1.75;
}

function updateProgressBar() {
   var progressBar = document.getElementById('progress-bar');
   var percentage = Math.floor((100 / player.duration) *
   player.currentTime);
   progressBar.value = percentage;
   progressBar.innerHTML = percentage + '% played';
}
